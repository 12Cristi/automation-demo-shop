package org.fasttrackit;

import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.Feature;
import io.qameta.allure.selenide.AllureSelenide;
import org.fasttrackit.pages.*;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;
import static org.fasttrackit.pages.DataSource.HOME_PAGE;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@Feature("Products features")
public class ProductsTest {

    DataSource constants;
    Cart cart;
    WishList wishList;
    BasePage basePage;

    @BeforeTest
    public void before() {
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide().screenshots(true).savePageSource(true));
        open(HOME_PAGE);
        cart = new Cart();
        constants = new DataSource();
        wishList = new WishList();
        basePage = new BasePage();
    }

    @Test
    public void verify_PageTitle() {
        assertTrue(basePage.isPageTitleDisplayed(), "Header Products displayed on page");
        assertEquals(basePage.pageTitleDescription(), "Products");
    }

    @Test(dataProvider = "productsData", dataProviderClass = DataSource.class)
    public void verify_productsOnPage(Product product) {
        assertTrue(product.isImageDisplayed(), "Product image is displayed");
        assertTrue(product.isDisplayed(), "Product " + product.getProductID() + " is displayed on page");
        assertEquals(product.url(), HOME_PAGE + product.getExpectedURL(), "Product " + product.getProductID() + " opened");
        assertEquals(product.getProductDescription(), product.getExpectedProductDescription(),
                "Product description accurate ");
        product.addToCart();
        assertTrue(cart.productsInCartBadge(), "After adding a productID to the basket the cart icon is updated\"");
        product.addToWishList();
        assertTrue(wishList.productWishList(), "After adding a productID to wishlist icon is updated\"");
        assertEquals(product.getProductPrice(), product.getExpectedProductPrice(), "Price displayed on page is accurate");
        assertEquals(product.getStock(), product.getExpectStock(), "Checking stock availability");
    }
}



