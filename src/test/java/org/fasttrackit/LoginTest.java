package org.fasttrackit;

import io.qameta.allure.Feature;
import org.fasttrackit.pages.Header;
import org.fasttrackit.pages.LoginModal;
import org.fasttrackit.pages.UserLogin;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;
import static org.fasttrackit.pages.DataSource.HOME_PAGE;
import static org.testng.Assert.assertEquals;

@Feature("Login Feature")
public class LoginTest {
    LoginModal loginModal;
    Header header;

    @BeforeTest
    public void openPage() {
        open(HOME_PAGE);
        loginModal = new LoginModal();
        header = new Header();
    }

    @Test(dataProvider = "data", dataProviderClass = UserLogin.class)
    public void verifyLogin(UserLogin userLogin) {
        header.loginClick();
        userLogin.username.sendKeys("dino");
        userLogin.password.sendKeys("choochoo");
        loginModal.loginButton.click();
        assertEquals(header.greetingsMessage(), userLogin.getUserLogin(), "Verify user logged in successfully");
        loginModal.logOutClick();
        assertEquals(header.greetingsMessage(), userLogin.getUserLogin(), "Verify user logged out successfully");
    }


}

