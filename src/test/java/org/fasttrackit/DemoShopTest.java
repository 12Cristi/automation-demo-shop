package org.fasttrackit;

import io.qameta.allure.Feature;
import org.fasttrackit.pages.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;
import static org.fasttrackit.pages.DataSource.HOME_PAGE;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@Feature("General features")
public class DemoShopTest {

    Header header;
    Footer footer;
    HelpModal helpModal;
    Cart cart;
    LoginModal loginModal;
    DataSource constants;
    BasePage basePage;

    @BeforeTest
    public void openPage() {
        open(HOME_PAGE);
        header = new Header();
        footer = new Footer();
        cart = new Cart();
        loginModal = new LoginModal();
        basePage = new BasePage();
        constants = new DataSource();
    }

    @AfterMethod
    public void cleanUp() {
        footer.resetApp();
    }

    @Test
    public void verify_loginModalOpens() {
        header.loginClick();
        assertTrue(loginModal.isLoginModalActive(), "Login Modal active and exists on page");
        loginModal.modalClose();
        assertTrue(loginModal.isLoginModalActive(), "After pressing X button Login Modal closes");

    }

    @Test
    public void verify_ProductsHeader() {
        assertTrue(constants.isPageTitleDisplayed(), "Header Products displayed on page");
        assertEquals(constants.pageTitleDescription(), "Products", "Product Title Content accurate");
    }

    @Test
    public void verify_searchField() {
        assertTrue(basePage.isSearchDisplayed(), "Search is displayed on page");
        assertEquals(basePage.isSearchFieldEmpty(), 0, "Checking if search field is empty");
        assertTrue(basePage.searchActive(), "Search active after click");
    }

    @Test
    public void verify_searchButton() {
        assertTrue(basePage.isSearchButtonDisplayed(), " Search button is displayed");
        assertEquals(basePage.getSearchButtonText(), "Search", "Getting text from Search Button");
    }

    @Test
    public void verify_sort() {
        assertTrue(basePage.isSortDisplayed(), "Sort dropdown is displayed");
        assertTrue(basePage.verifySortDefaultSelected(), "Sort by name(A to Z) default selected");
        assertEquals(basePage.sortingZtoA(), "Sort by name (Z to A)", "Sorting from Z to A ");
        assertEquals(basePage.sortingLowToHigh(), "Sort by price (low to high)", "Sorting from low to high");
        assertEquals(basePage.sortingHighToLow(), "Sort by price (high to low)", "Sorting from high to low");
        assertEquals(basePage.sortingAtoZ(), "Sort by name (A to Z)", "Sorting from Z to A ");
    }


    @Test
    public void verify_footerDemoShopLink() {
        assertTrue(footer.isDemoShopLinkDisplayed(), "DemoShop Footer link exists and displayed ");
        assertEquals(footer.demoShopMessage(), "Demo Shop | build date 2021-05-21 14:04:30 GTBDT", "Footer DemoShop details  correct");
        assertEquals(footer.clickedDemoShopLink(), HOME_PAGE, "After clicking DemoShop Footer link redirected to HomePage");
    }

    @Test
    public void verify_helModalOpen() {
        helpModal = new HelpModal();
        helpModal.open();
        assertTrue(footer.isQuestionIconDisplayed(), "Question icon is displayed");
        assertEquals(helpModal.isActive(), "true", "Help Modal is active");
    }

    @Test
    public void verify_helpModalClose() {
        helpModal = new HelpModal();
        helpModal.open();
        assertEquals(helpModal.isActive(), "true", "Help Modal is active");
//        helpModal.close();
        assertTrue(helpModal.status(), "Help Modal is  not active");

    }

    @Test
    public void verify_helpModalContent() {
        helpModal = new HelpModal();
        helpModal.open();
        assertEquals(helpModal.title(), "Help", "HelpModal title details correct");
        assertTrue(helpModal.status(), "HelpModal content displayed");
        assertTrue(helpModal.isValidUsernamesTableDisplayed(), "HelpModal table displayed");
        assertEquals(helpModal.validUsernamesTable(), """
                Valid usernames
                Username Type Password
                dino normal user choochoo
                beetle user with bugs choochoo
                turtle slow user choochoo
                locked locked out user choochoo""", "Details containing Valid usernames on helpModal");
        helpModal.close();
    }

    @Test
    public void verify_resetApplication() {
        assertTrue(footer.isResetButtonDisplayed(), "Reset application button displayed");
        assertEquals(footer.resetAppHoverText(), "Reset the application state", "Reset application description");
    }
}


