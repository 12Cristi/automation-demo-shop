package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;
import org.testng.annotations.DataProvider;

import static com.codeborne.selenide.Selenide.$;
import static java.lang.String.format;

public class UserLogin {
    public SelenideElement username = $("#user-name");
    public SelenideElement password = $("#password");
    public SelenideElement userLogged = $(".shopping-cart-icon ~ span span");

    public String user;
    public String pass;

    public static final UserLogin DINO = new UserLogin("dino");
    public static final UserLogin BEETLE = new UserLogin("turtle");
    public static final UserLogin TURTLE = new UserLogin("turtle");
    public static final UserLogin LOCKED = new UserLogin("locked");


    public UserLogin(String UserLogin) {
        this.username = $(format(("#user-name"), UserLogin));
    }

    @DataProvider(name = "data")
    public static Object[][] getUser() {
        Object[][] credential = new Object[4][];
        credential[0] = new Object[]{DINO};
        credential[1] = new Object[]{BEETLE};
        credential[2] = new Object[]{TURTLE};
        credential[3] = new Object[]{LOCKED};
        return credential;
    }

    public String getUserLogin() {
        return userLogged.getText();
    }
}
