package org.fasttrackit.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class BasePage {
    private final SelenideElement resetAppButton = $(".btn-link .fa-undo");
    private final SelenideElement modalCloseButton = $(".close");
    private final SelenideElement searchField = $("#input-search");
    private final String searchFieldEmpty = searchField.getText();
    private final SelenideElement search = $("#input-search");
    private final SelenideElement searchButton = $(".col-md-auto [type=button]");
    private final SelenideElement sort = $(".sort-products-select");
    private final SelenideElement sortAZ = $(".sort-products-select [value=az]");
    private final SelenideElement sortZA = $(".sort-products-select [value=za]");
    private final SelenideElement sortLoHi = $(".sort-products-select [value=lohi]");
    private final SelenideElement sortHiLo = $(".sort-products-select [value=hilo]");
    private final boolean sortDefaultSelected = sortAZ.isSelected();
    private final SelenideElement productsHeaderTitle = $(".subheader-container .text-muted");


    public void refresh() {
        Selenide.refresh();
    }

    public boolean isPageTitleDisplayed() {
        return productsHeaderTitle.exists();
    }

    public String pageTitleDescription() {
        return productsHeaderTitle.getText();
    }


    public boolean isSearchDisplayed() {
        return searchField.exists();
    }

    public boolean searchActive() {
        return search.isEnabled();
    }

    public boolean isSearchButtonDisplayed() {
        return searchButton.exists();
    }

    public String getSearchButtonText() {
        return searchButton.getOwnText();
    }

    public Integer isSearchFieldEmpty() {
        return searchFieldEmpty.length();
    }

    public boolean isSortDisplayed() {
        return sort.exists();
    }

    public boolean verifySortDefaultSelected() {
        return sortDefaultSelected;
    }

    public String sortingAtoZ() {
        return sortAZ.getText();
    }

    public String sortingZtoA() {
        return sortZA.getText();
    }

    public String sortingHighToLow() {
        return sortHiLo.getText();
    }

    public String sortingLowToHigh() {
        return sortLoHi.getText();
    }

    public void resetApp() {
        if (modalCloseButton.exists() && modalCloseButton.isDisplayed()) {
            modalClose();
        } else {
            resetAppButton.click();
        }
    }

    public void modalClose() {
        modalCloseButton.click();
    }

    public boolean isResetButtonDisplayed() {
        return resetAppButton.isDisplayed() && resetAppButton.exists();
    }
}

