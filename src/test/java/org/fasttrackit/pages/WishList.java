package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class WishList extends BasePage {

    private final SelenideElement wishlistBadge = $("[data-icon=heart]~.shopping_cart_badge");

    public boolean productWishList() {
        return wishlistBadge.exists();
    }
}
