package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static org.fasttrackit.pages.DataSource.HOME_PAGE;


public class Footer extends BasePage {
    private final SelenideElement navLink = $(".nav-link");
    private final SelenideElement questionIcon = $(".btn-link [data-icon=question]");
    private final SelenideElement resetAppButton = $(".btn-link .fa-undo");
    private final String text = resetAppButton.parent().getAttribute("title");


    public boolean isDemoShopLinkDisplayed() {
        return navLink.exists() && navLink.isDisplayed();
    }

    public String demoShopMessage() {
        return navLink.getText();
    }

    public String clickedDemoShopLink() {
        return HOME_PAGE;
    }

    public boolean isQuestionIconDisplayed() {
        return questionIcon.isDisplayed();
    }

    public boolean isResetButtonDisplayed() {
        return resetAppButton.isDisplayed() && resetAppButton.exists();
    }

    public String resetAppHoverText() {
        return text;
    }

}
