package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static java.lang.String.format;

public class Product extends BasePage {
    private final SelenideElement title;
    private final SelenideElement price;
    private final SelenideElement productDescription;
    private final String expectedProductPrice;
    private final String expectedProductDescription;
    private final String expectedURL;
    private final String productID;
    private final String expectStock;
    private final SelenideElement stock;

    public Product(String productId, String price, String productDescription, String stock) {
        this.productID = productId;
        this.title = $(format("a[href='#/product/%s']", productId));
        this.expectedURL = format("#/product/%s", productId);
        this.price = title.parent().parent().$(".card-footer .card-text");
        this.expectedProductPrice = price;
        this.productDescription = title.parent().parent().$(".card-text");
        this.expectedProductDescription = productDescription;
        this.stock = title.parent().parent().$(".card-text small");
        this.expectStock = stock;

    }

    public String getProductID() {
        return productID;
    }

    public boolean isDisplayed() {
        return title.exists();
    }

    public String url() {
        return title.getAttribute("href");
    }

    public String getExpectedURL() {
        return this.expectedURL;
    }

    public void addToCart() {
        SelenideElement parent = title.parent().parent();
        SelenideElement addToCartIcon = parent.$("[data-icon=cart-plus]");
        addToCartIcon.click();
    }

    public void addToWishList() {
        SelenideElement parent = title.parent().parent();
        SelenideElement addToWishlist = parent.$("[data-icon=heart]");
        addToWishlist.click();
    }

    public String getProductDescription() {
        return this.productDescription.getText();
    }

    public String getExpectedProductDescription() {
        return this.expectedProductDescription;
    }

    public String getExpectedProductPrice() {
        return this.expectedProductPrice;
    }

    public String getProductPrice() {
        return this.price.getText();
    }

    public String getStock() {
        return this.stock.getText();
    }

    public String getExpectStock() {
        return expectStock;
    }

    public boolean isImageDisplayed() {
        SelenideElement parent = title.parent().parent();
        final SelenideElement image = parent.$(".card-img");
        return image.exists();
    }

}

