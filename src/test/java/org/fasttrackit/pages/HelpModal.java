package org.fasttrackit.pages;
import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.$;


public class HelpModal extends BasePage {
    private final SelenideElement helpModalTitle = $(" .modal-content .text-muted");
    private final SelenideElement close = $(".close");
    private final SelenideElement questionIcon = $(".nav-item [data-icon=question]");
    private final SelenideElement table = $(" .modal-content .table");
    private final SelenideElement modalOpen = $("#root");

    public void open() {
        questionIcon.click();
    }

    public String title() {
        return helpModalTitle.getText();
    }

    public String isActive() {
        return modalOpen.getAttribute("aria-hidden");
    }

    public void close() {
        close.click();
    }

    public boolean status() {
        return helpModalTitle.isDisplayed();
    }

    public boolean isValidUsernamesTableDisplayed() {
        return table.isDisplayed() && table.exists();
    }

    public String validUsernamesTable() {
        return table.parent().getText();
    }
}
