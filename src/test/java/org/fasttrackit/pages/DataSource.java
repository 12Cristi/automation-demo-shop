package org.fasttrackit.pages;


import com.codeborne.selenide.SelenideElement;
import org.testng.annotations.DataProvider;

import static com.codeborne.selenide.Selenide.$;

public class DataSource extends BasePage {
    public static final String HELLO_GUEST_GREETINGS_MSG = "Hello guest!";
    public static final String HOME_PAGE = "https://fasttrackit-test.netlify.app/";
    public static final String AWESOME_GRANITE_CHIPS_PRODUCT_ID = "1";
    public static final String AWESOME_METAL_CHAIR_PRODUCT_ID = "3";
    public static final String AWESOME_SOFT_SHIRT_PRODUCT_ID = "5";
    public static final String GORGEOUS_SOFT_PIZZA_PRODUCT_ID = "9";
    public static final String INCREDIBLE_CONCRETE_HAT_PRODUCT_ID = "2";
    public static final String LICENSED_STEEL_GLOVES_PRODUCT_ID = "8";
    public static final String PRACTICAL_METAL_MOUSE_PRODUCT_ID = "7";
    public static final String PRACTICAL_WOODEN_BACON_PRODUCT_ID = "4";
    public static final String PRACTICAL_WOODEN_BACON_PRODUCT_IDS = "6";
    public static final String REFINED_FROZEN_MOUSE_PRODUCT_ID = "0";

    public static final Product AWESOME_GRANITE_CHIPS_PRODUCT = new Product(AWESOME_GRANITE_CHIPS_PRODUCT_ID, "$15.99",
            "Omnis excepturi laudantium et minima dignissimos e", "in stock");
    public static final Product AWESOME_METAL_CHAIR_PRODUCT = new Product(AWESOME_METAL_CHAIR_PRODUCT_ID, "$15.99",
            "Adipisci velit optio dolorem minima ex. Dolorem ma", "in stock");
    public static final Product AWESOME_SOFT_SHIRT_PRODUCT = new Product(AWESOME_SOFT_SHIRT_PRODUCT_ID, "$29.99",
            "Qui veniam laboriosam quia sequi ab. Dolorem eveni", "in stock");
    public static final Product GORGEOUS_SOFT_PIZZA_PRODUCT = new Product(GORGEOUS_SOFT_PIZZA_PRODUCT_ID, "$19.99",
            "Qui veniam laboriosam quia sequi ab. Dolorem eveni", "in stock");
    public static final Product INCREDIBLE_CONCRETE_HAT_PRODUCT = new Product(INCREDIBLE_CONCRETE_HAT_PRODUCT_ID, "$7.99",
            "Asperiores est error. Eum dicta totam. Aut minima", "in stock");
    public static final Product LICENSED_STEEL_GLOVES_PRODUCT = new Product(LICENSED_STEEL_GLOVES_PRODUCT_ID, "$14.99",
            "Qui veniam laboriosam quia sequi ab. Dolorem eveni", "in stock");
    public static final Product PRACTICAL_METAL_MOUSE_PRODUCT = new Product(PRACTICAL_METAL_MOUSE_PRODUCT_ID, "$9.99",
            "Qui veniam laboriosam quia sequi ab. Dolorem eveni", "in stock");
    public static final Product PRACTICAL_WOODEN_BACON_PRODUCT_1 = new Product(PRACTICAL_WOODEN_BACON_PRODUCT_ID, "$29.99",
            "Qui veniam laboriosam quia sequi ab. Dolorem eveni", "in stock");
    public static final Product PRACTICAL_WOODEN_BACON_PRODUCT_2 = new Product(PRACTICAL_WOODEN_BACON_PRODUCT_IDS, "$1.99",
            "Qui veniam laboriosam quia sequi ab. Dolorem eveni", "in stock");
    public static final Product REFINED_FROZEN_MOUSE_PRODUCT = new Product(REFINED_FROZEN_MOUSE_PRODUCT_ID, "$9.99",
            "Vero quia nesciunt laborum velit. Et in ipsum quia", "in stock");


    private final SelenideElement loginIcon = $(".fa-sign-in-alt");

    @DataProvider(name = "productsData")
    public static Object[][] getProductData() {
        Object[][] products = new Object[10][];
        products[0] = new Object[]{REFINED_FROZEN_MOUSE_PRODUCT};
        products[1] = new Object[]{AWESOME_GRANITE_CHIPS_PRODUCT};
        products[2] = new Object[]{INCREDIBLE_CONCRETE_HAT_PRODUCT};
        products[3] = new Object[]{AWESOME_METAL_CHAIR_PRODUCT};
        products[4] = new Object[]{PRACTICAL_WOODEN_BACON_PRODUCT_1};
        products[5] = new Object[]{AWESOME_SOFT_SHIRT_PRODUCT};
        products[6] = new Object[]{PRACTICAL_WOODEN_BACON_PRODUCT_2};
        products[7] = new Object[]{PRACTICAL_METAL_MOUSE_PRODUCT};
        products[8] = new Object[]{LICENSED_STEEL_GLOVES_PRODUCT};
        products[9] = new Object[]{GORGEOUS_SOFT_PIZZA_PRODUCT};
        return products;
    }
}


