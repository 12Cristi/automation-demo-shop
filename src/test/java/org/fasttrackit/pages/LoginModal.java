package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;


public class LoginModal extends BasePage {
    private final SelenideElement loginModal = $(".login_wrapper");
    public final SelenideElement loginButton = $(".btn-primary");
    private final SelenideElement logOutIcon = $(".fa-sign-out-alt");

    public boolean isLoginModalActive() {
        return loginModal.exists();
    }

    public void logOutClick() {
        logOutIcon.click();
    }
}
