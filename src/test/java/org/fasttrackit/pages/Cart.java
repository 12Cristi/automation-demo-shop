package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;


public class Cart extends BasePage {
    private final SelenideElement cartTitle = $(".subheader-container .text-muted");
    private final SelenideElement emptyCart = $(".text-center ");
    private final SelenideElement shoppingCartBadge = $("[data-icon=shopping-cart]~.shopping_cart_badge");

    public String getCartPageHeader() {
        return cartTitle.getText();
    }

    public String isCartEmpty() {
        return emptyCart.getText();
    }

    public boolean cartEmpty() {
        return emptyCart.isDisplayed();
    }

    public boolean productsInCartBadge() {
        return shoppingCartBadge.exists();
    }

}


