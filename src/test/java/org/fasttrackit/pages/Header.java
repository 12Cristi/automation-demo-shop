package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Header extends BasePage {

    private final SelenideElement greetings = $(".navbar-text > span");
    private final SelenideElement logo = $(".navbar-brand .fa-shopping-bag");
    private final String logoHoverText = logo.parent().getAttribute("href");
    private final SelenideElement favoritesIcon = $(".navbar-nav [data-icon=heart]");
    private final SelenideElement loginIcon = $(".fa-sign-in-alt");
    private final SelenideElement logOutIcon = $(".fa-sign-out-alt");
    private final SelenideElement loginModal = $(".modal-content");
    private final SelenideElement cartIcon = $(".shopping-cart-icon [data-icon=shopping-cart] ");


    public String greetingsMessage() {
        return greetings.getText();
    }

    public boolean isGreetingsMessageDisplayed() {
        return greetings.exists();
    }

    public boolean isLogoDisplayed() {
        return logo.exists();
    }

    public String logoLinkOnHover() {
        return logoHoverText;
    }

    public String logoRedirectUrl() {
        return logo.parent().getAttribute("href");
    }

    public boolean isCartIconDisplayed() {
        return cartIcon.exists();
    }

    public String cartRedirectUrl() {
        return cartIcon.parent().getAttribute("href");
    }

    public boolean isFavoritesIconDisplayed() {
        return favoritesIcon.exists();
    }

    public String wishListRedirectURL() {
        return favoritesIcon.parent().getAttribute("href");
    }

    public boolean isLoginIconDisplayed() {
        return loginIcon.exists();
    }

    public void loginClick() {
        loginIcon.click();
    }

    public boolean isLoginModalDisplayed() {
        return loginModal.exists();
    }


}

