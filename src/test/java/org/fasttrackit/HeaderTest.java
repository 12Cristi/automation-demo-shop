package org.fasttrackit;

import io.qameta.allure.Feature;
import org.fasttrackit.pages.Cart;
import org.fasttrackit.pages.Header;
import org.fasttrackit.pages.WishList;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;
import static org.fasttrackit.pages.DataSource.HELLO_GUEST_GREETINGS_MSG;
import static org.fasttrackit.pages.DataSource.HOME_PAGE;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@Feature("Header feature")
public class HeaderTest {

    Header header;
    WishList wishList;
    Cart cart;


    @BeforeTest
    public void openPage() {
        open(HOME_PAGE);
        header = new Header();
        wishList = new WishList();
        cart = new Cart();
    }

    @Test
    public void verify_greetingMessage() {
        assertTrue(header.isGreetingsMessageDisplayed(), "Welcome message must be displayed on page.");
        assertEquals(header.greetingsMessage(), HELLO_GUEST_GREETINGS_MSG, "Welcome message correct and it is displayed.");
    }

    @Test
    public void verify_homePageLogoURL() {
        assertTrue(header.isLogoDisplayed(), "Logo element exists in navigation bar");
        assertEquals(header.logoLinkOnHover(), "https://fasttrackit-test.netlify.app/", "Hoover on logo display HomePage link");
        assertEquals(header.logoRedirectUrl(), HOME_PAGE, "Clicking the Logo, redirects to homepage");
    }

    @Test
    public void verify_wishlistURL() {
        assertTrue(header.isFavoritesIconDisplayed(), "Wishlist icon is displayed");
        assertEquals(header.wishListRedirectURL(), HOME_PAGE + "#/wishlist", "Page redirected to Wishlist Page");
    }

    @Test
    public void verify_cartURL() {
        assertTrue(header.isCartIconDisplayed(), "Cart icon is displayed on page");
        assertEquals(header.cartRedirectUrl(), HOME_PAGE + "#/cart", "Redirected to Your Cart Page");
    }

    @Test
    public void verify_loginIcon() {
        assertTrue(header.isLoginIconDisplayed(), "Login icon exist and displayed on page!");
        header.loginClick();
        assertTrue(header.isLoginModalDisplayed(), "Login modal active");
    }

}
